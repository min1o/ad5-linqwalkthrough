# Dataset querying met LINQ

**Author:** Ingabire Marie-Aimée

**Course:**  AD5

**AJ:**  2015-2016

**Repository:** [AD5-LinqWalkthrough](https://bitbucket.org/neit1o/ad5-linqwalkthrough)

## Publiek

Gevorderd

## Onderdelen
1. Voorbereiding
2. Basis CRUD operaties
3. Uitbreiden en verbeteren
4. Foreign Key Constraints
5. Testen
6. Extensies

## Vereisten
De walkthrough is gemaakt en getest op Visual Studio 2015 (Ultimate edition). Het kan zijn dat sommige operaties niet werken op andere versies.


### Walkthrough

### Voorbereiding

Om te beginnen zullen we een database moeten opstellen. Voor deze walkthrough hebben zullen we ons houden aan locale databanken.
Vooraleer we hieraan kunnen beginnen moeten we eerste een project aanmaken.

Ga naar `File > New > Project`. Kies dan voor een **Console Application**, noem het *DatasetsMetLINQ*. De naam maakt opzich niet uit.

#### Locale database aanmaken

Visual studio laat u toe om locale databanken aan te maken. In deze walkthrough zullen we gebruiken maken van een *MS SQL Server Database File*. De locale databanken hebben een `.mdf` extensie en hebben T-SQL als querytaal.
Om een nieuwe data source toe te voegen kan je naar `Project > Add New Data Source...` gaan. 

Voeg een MS SQL Server Database File toe met de naam 'DBLinq.mdf' en een ConnectionString met de naam 'DBLinqCS'.
Let er ook op dat de locale database toegevoegd wordt aan het project zodat het toegangkelijk is via de *Solution Explorer*.

#### Stuctuur aanmaken

Vooraleer je met de database gaan kunnen interageren moet je eerst de structuur van de database aanmaken. Dit kan via de *Server Explorer* van Visual Studio.

Druk `CTRL + W, L` of ga naar `View > Server Explorer`.
Rechtsklik op 'DBLinq.mdf' en kies dan voor 'Refresh'.

##### Structuur

Rechtsklik op 'DBLinq.mdf' en kies nu voor 'New Query'. Kopieer wat hieronder is in de editor, klik dan op 'Execute'.
Als je nu opniew een refresh doe van de databank zou je 2 tabellen moeten hebben.

Het script zal de tabellen `tblPersons` en `tblGroups`. Dit is een simpele structuur, een groep heeft nul of meedere gebruikers en een gebruiker behoort altijd tot een group.

##### Script

```

	/*
	* ----------------------------------------- TABLES
	*/
	if not exists (select name from sys.tables where name = 'tblPersons')
	begin
		CREATE TABLE tblPersons
		(
			Id              INT NOT NULL            IDENTITY(1,1),
			FirstName   NVARCHAR(50) NOT NULL,
			LastName    NVARCHAR(50) NOT NULL,
			Email       NVARCHAR(50) NOT NULL
		)
	end
	go
	if not exists (select name from sys.tables where name = 'tblGroupMembers')
	begin
		CREATE TABLE tblGroupMembers
		(
			PER_ID      INT NOT NULL,
			GRP_ID      INT NOT NULL
		)
	end
	go
	if not exists (select name from sys.tables where name = 'tblGroups')
	begin   
		CREATE TABLE tblGroups
		(
			Id          INT NOT NULL            IDENTITY(1,1),
			Title       NVARCHAR(50) NOT NULL
		)
	end
	go

	/*
	* ----------------------------------------- CONSTRAINTS
	*/

	if not exists (select CONSTRAINT_NAME from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where CONSTRAINT_NAME = 'Groups_PK')
	begin
		ALTER TABLE tblGroups
			ADD
				CONSTRAINT Groups_PK                PRIMARY KEY(Id),
				CONSTRAINT Groups_UQ_title          UNIQUE(Title)
	end
	go

	if not exists (select CONSTRAINT_NAME from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where CONSTRAINT_NAME = 'Persons_PK')
	begin
		ALTER TABLE tblPersons
			ADD
				CONSTRAINT Persons_PK               PRIMARY KEY(Id),
				CONSTRAINT Persons_UQ_email         UNIQUE(Email)
	end
	go

	if not exists (select CONSTRAINT_NAME from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where CONSTRAINT_NAME = 'GroupMembers_PK' OR CONSTRAINT_NAME = 'GroupMembers_FK_persons' OR CONSTRAINT_NAME = 'GroupMembers_FK_groups')
	begin
		ALTER TABLE tblGroupMembers
			ADD
				CONSTRAINT GroupMembers_PK          PRIMARY KEY(PER_ID, GRP_ID),
				CONSTRAINT GroupMembers_FK_persons  FOREIGN KEY(PER_ID) REFERENCES tblPersons(Id),
				CONSTRAINT GroupMembers_FK_groups   FOREIGN KEY(GRP_ID) REFERENCES tblGroups(Id)
	end
	go


```

#### Databank verbinden met applicatie

Om te beginnen moet je een nieuwe klasse aanmaken. Druk `SHIFT + ALT + C` of rechtsklik op het project, kies dan `Add > Class...`. Noem het *DbAccess* en klik dan op 'Add'.

```

	namespace DatasetsMetLINQ
	{
		public class DbAccess
		{
			private static readonly SqlConnection _dbConnection;

			static DbAccess()
			{
				String connectionString = Properties.Settings.Default.DBLinqCS ?? "";
				_dbConnection = new SqlConnection(connectionString);

				if (!string.IsNullOrEmpty(_dbConnection.ConnectionString))
				{} else {
					Console.WriteLine("Error could not connect to the database.");
				}
			}
		}
	}

```

#### Voeg een Connect en Disconnect functie toe

```

		private static void Connect()
		{
			if (_dbConnection.State == ConnectionState.Closed)
			{
				_dbConnection.Open();
			}
		}
		private static void Disconnect()
		{
			if (_dbConnection.State != ConnectionState.Closed)
			{
				_dbConnection.Close();
			}
		}


```

#### Implementeer verbinding met dataset

```

	private static void InitDatasets()
	{
		SqlCommandBuilder cmdBuilderNot;
		// ------------------- DATASETS
		DataSet = new DataSet("AppDataset");

		// ------------------- ADAPTERS
		_PersonsAdapter = new SqlDataAdapter("SELECT * FROM tblPersons", _DbConnection);
		_PersonsAdapter.TableMappings.Add("Table", "tblPersons");
		_PersonsAdapter.Fill(DataSet);

		_GroupMembersAdapter = new SqlDataAdapter("SELECT * FROM tblGroupMembers", _DbConnection);
		_GroupMembersAdapter.TableMappings.Add("Table", "tblGroupMembers");
		_GroupMembersAdapter.Fill(DataSet);

		_GroupsAdapter = new SqlDataAdapter("SELECT * FROM tblGroups", _DbConnection);
		_GroupsAdapter.TableMappings.Add("Table", "tblGroups");
		_GroupsAdapter.Fill(DataSet);


		// ------------------- SETUP COMMANDS
		cmdBuilderNot = new SqlCommandBuilder(_PersonsAdapter);
		_PersonsAdapter.InsertCommand = cmdBuilderNot.GetInsertCommand();
		_PersonsAdapter.DeleteCommand = cmdBuilderNot.GetDeleteCommand();
		_PersonsAdapter.UpdateCommand = cmdBuilderNot.GetUpdateCommand();

		cmdBuilderNot = new SqlCommandBuilder(_GroupMembersAdapter);
		_GroupMembersAdapter.InsertCommand = cmdBuilderNot.GetInsertCommand();
		_GroupMembersAdapter.DeleteCommand = cmdBuilderNot.GetDeleteCommand();
		_GroupMembersAdapter.UpdateCommand = cmdBuilderNot.GetUpdateCommand();

		cmdBuilderNot = new SqlCommandBuilder(_GroupsAdapter);
		_GroupsAdapter.InsertCommand = cmdBuilderNot.GetInsertCommand();
		_GroupsAdapter.DeleteCommand = cmdBuilderNot.GetDeleteCommand();
		_GroupsAdapter.UpdateCommand = cmdBuilderNot.GetUpdateCommand();

		// ------------------- RELATIONS

		DataColumn colParent;
		DataColumn colChild;
		colParent = DataSet.Tables["tblGroups"].Columns["Id"];
		colChild = DataSet.Tables["tblGroupMembers"].Columns["GRP_ID"];
		RelationGroupMemberToGroup = new DataRelation("RelationGroupMemberToGroup", colParent, colChild);
		DataSet.Relations.Add(RelationGroupMemberToGroup);

		colParent = DataSet.Tables["tblPersons"].Columns["Id"];
		colChild = DataSet.Tables["tblGroupMembers"].Columns["PER_ID"];
		RelationGroupMemberToPerson = new DataRelation("RelationGroupMemberToPerson", colParent, colChild);
		DataSet.Relations.Add(RelationGroupMemberToPerson);
	}

```

Roept dit nu op in de constructor van de database.

```

	static DbAccess()
	{
		...

		if (!string.IsNullOrEmpty(_dbConnection.ConnectionString))
		{
			InitDatasets();
		}
		...
	}

```

Er moeten ook functies bestaan om veranderingen te bewaren in de databank.

```

	private static void SaveChangesToDb()
	{

		try
		{
			Connect();

			_PersonsAdapter.Update(DataSet, "tblPersons");
			_GroupsAdapter.Update(DataSet, "tblGroups");
			_GroupMembersAdapter.Update(DataSet, "tblGroupMembers");

			Disconnect();
		}
		catch (Exception ex)
		{
			Console.WriteLine(ex.Message, "ERROR SaveChanges");
		}
		finally
		{
			UpdateDataset();
		}
	}

	private static void UpdateDataset()
	{
		Connect();

		DataSet.AcceptChanges();
		DataSet.Clear();
		DataSet.EnforceConstraints = false;
		_PersonsAdapter.Fill(DataSet);
		_GroupsAdapter.Fill(DataSet);
		_GroupMembersAdapter.Fill(DataSet);
		DataSet.EnforceConstraints = true;

		Disconnect();
	}

```
#### Klassen

Maak de volgende klassen.

```


	public class Person
	{
		public int Id { get; set; }
		public string Email { get; set; }
		public string LastName { get; set; }
		public string FirstName { get; set; }

		public Person():this(-1, String.Empty, String.Empty, String.Empty)
		{}
		public Person(int id, String firstname, String lastname, String email)
		{
			Id = id;
			FirstName = firstname;
			LastName = lastname;
			Email = email;
		}

		public override string ToString()
		{
			return $"[{Id}] {FirstName}, {LastName}";
		}
	}
	public class GroupMember
	{
		public Person Person { get; set; }
		public Group Group { get; set; }

		public GroupMember(): this(new Person(), new Group())
		{}
		public GroupMember(Person person, Group group)
		{
			Person = person;
			Group = group;
		}

	}
	public class Group
	{
		public int Id { get; set; }
		public String Title { get; set; }
		public List<GroupMember> AllGroupMembers { get; set; }

		public Group(): this(-1, String.Empty)
		{}
		public Group(int id, String title)
		{
			Id = id;
			Title = title;
		}

		public override string ToString()
		{
			int count = AllGroupMembers?.Count ?? 0;

			String msg = $"[{Id}] {Title} - #:{count}";

			if (count > 0)
			{
				msg += "\n\t\t\t";
				AllGroupMembers?
					.ForEach(gm => msg += $"> {gm.Person.FirstName} {gm.Person.LastName}" + "\n\t\t\t");
			}
			
			return msg;
		}
	}

```

### Basis CRUD operaties

```

		public static List<Person> GetAllPersons()
		{
			List<Person> lstPersons = new List<Person>();

			lstPersons = DataSet.Tables["tblPersons"]
				.AsEnumerable()
				.Select(dataRow => new Person(
					dataRow.Field<int>("Id"), 
					dataRow.Field<String>("FirstName"), 
					dataRow.Field<String>("LastName"), 
					dataRow.Field<String>("Email")))
				.ToList();
			return lstPersons;
		}
		public static void AddPerson(Person person)
		{
			try
			{
				DataRow newRow = DataSet.Tables["tblPersons"].NewRow();

				newRow["FirstName"] = person.FirstName;
				newRow["LastName"] = person.LastName;
				newRow["Email"] = person.Email;

				DataSet.Tables["tblPersons"].Rows.Add(newRow);
			}
			catch (Exception ex)
			{
				Console.WriteLine("ERROR AddPerson" + ex.Message);
			}
			finally
			{
				SaveChangesToDb();
			}
		}
		public static void RemovePerson(Person person)
		{
			DataRow row;
			try
			{
				row = DataSet.Tables["tblPersons"]
					.AsEnumerable()
					.FirstOrDefault(dataRow => dataRow.Field<int>("Id") == person.Id);

				row?.Delete();
			}
			catch (Exception ex)
			{
				Console.WriteLine("ERROR RemovePerson" + ex.Message);
			}
			finally
			{
				SaveChangesToDb();
			}
		}
		public static void EditPerson(Person person)
		{
			DataRow row;
			try
			{
				row = DataSet.Tables["tblPersons"]
					.AsEnumerable()
					.FirstOrDefault(dataRow => dataRow.Field<String>("Email") == person.Email);
				
				row?.SetField<String>("FirstName", person.FirstName);
				row?.SetField<String>("LastName", person.LastName);
				row?.SetField<String>("Email", person.Email);
			}
			catch (Exception ex)
			{
				Console.WriteLine("ERROR EditPerson" + ex.Message);
			}
			finally
			{
				SaveChangesToDb();
			}
		}

		public static List<Group> GetAllGroups()
		{
			List<Group> lstGroups = new List<Group>();

			lstGroups = DataSet.Tables["tblGroups"]
				.AsEnumerable()
				.Select(dataRow => new Group(
					dataRow.Field<int>("Id"),
					dataRow.Field<String>("Title")
				 ))
				.ToList();

			return lstGroups;
		}
		public static void AddGroup(Group testGroup)
		{
			try
			{
				DataRow newRow = DataSet.Tables["tblGroups"].NewRow();

				newRow["Title"] = testGroup.Title;

				DataSet.Tables["tblGroups"].Rows.Add(newRow);
			}
			catch (Exception ex)
			{
				Console.WriteLine("ERROR AddGroup" + ex.Message);
			}
			finally
			{
				SaveChangesToDb();
			}
		}
		public static void EditGroup(Group group)
		{
			DataRow row;
			try
			{
				row = DataSet.Tables["tblGroups"]
					.AsEnumerable()
					.FirstOrDefault(dataRow => dataRow.Field<int>("Id") == group.Id);

				row?.SetField<String>("Title", group.Title);
			}
			catch (Exception ex)
			{
				Console.WriteLine("ERROR EditGroup" + ex.Message);
			}
			finally
			{
				SaveChangesToDb();
			}
		}
		public static void RemoveGroup(Group group)
		{
			DataRow row;
			try
			{
				row = DataSet.Tables["tblGroups"]
					.AsEnumerable()
					.FirstOrDefault(dataRow => dataRow.Field<int>("Id") == group.Id);

				row?.Delete();
			}
			catch (Exception ex)
			{
				Console.WriteLine("ERROR EditGroup" + ex.Message);
			}
			finally
			{
				SaveChangesToDb();
			}
		}
```

Dit zal werken om de groepen en personen te vinden, aanpassen en verwijderen maar we kunnen momenteel niet aan de groepsleden. We houden ook geen rekening met *Foreign Key Constraints*.

### Uitbreiden en verbeteren

```

	public static List<Group> GetAllGroups()
	{
		List<Group> lstGroups = new List<Group>();

		lstGroups = DataSet.Tables["tblGroups"]
			.AsEnumerable()
			.Select(dataRow => new Group(
				dataRow.Field<int>("Id"),
				dataRow.Field<String>("Title")
			 ))
			.ToList();

		lstGroups
			.ForEach(g => g.AllGroupMembers = GetAllMembersInGroup(g));

		return lstGroups;
	}

```
```

	public static List<GroupMember> GetAllGroupMembers()
	{
		List<GroupMember> lstGroupMembers = new List<GroupMember>();

		lstGroupMembers = DataSet.Tables["tblGroupMembers"]
			.AsEnumerable()
			.Select(row => new GroupMember(
				GetPersonById(row.Field<int>("PER_ID")),
				GetGroupById(row.Field<int>("GRP_ID"))
				)
			 ).ToList();

		return lstGroupMembers;
	}
	public static List<GroupMember> GetAllMembersInGroup(Group group)
	{
		List<GroupMember> lst = new List<GroupMember>();
		
		DataRow groupDataRow = DataSet.Tables["tblGroups"]
			.AsEnumerable()
			.FirstOrDefault(g => g.Field<int>("Id") == group.Id);

		DataRow[] allGroupMembers = groupDataRow?.GetChildRows(RelationGroupMemberToGroup);

		lst = allGroupMembers?.Select(gmRow => new GroupMember(
				GetPersonById(gmRow.Field<int>("PER_ID")),
				group 
			)).ToList();

		return lst;
	}

	private static Group GetGroupById(int id)
	{
		return GetAllGroups().FirstOrDefault(g => g.Id == id);
	}

	private static Person GetPersonById(int id)
	{
		return GetAllPersons().FirstOrDefault(p => p.Id == id);
	}


```

```

	public static void AddPersonToGroup(Person person, Group group)
	{
		try
		{
			DataRow newRow = DataSet.Tables["tblGroupMembers"].NewRow();

			newRow["PER_ID"] = person.Id;
			newRow["GRP_ID"] = group.Id;

			DataSet.Tables["tblGroupMembers"].Rows.Add(newRow);
		}
		catch (Exception ex)
		{
			Console.WriteLine("ERROR AddPersonToGroup" + ex.Message);
		}
		finally
		{
			SaveChangesToDb();
		}

	}

	public static void RemovePersonFromGroup(Person person, Group group)
	{
		DataRow row;
		try
		{
			row = DataSet.Tables["tblGroupMembers"]
				.AsEnumerable()
				.FirstOrDefault( dataRow => dataRow.Field<int>("PER_ID") == person.Id 
											&& dataRow.Field<int>("GRP_ID") == group.Id
				 );

			row?.Delete();
		}
		catch (Exception ex)
		{
			Console.WriteLine("ERROR RemovePerson" + ex.Message);
		}
		finally
		{
			SaveChangesToDb();
		}
	}

```

### Foreign Key Constraints

Als we nu zoudet proberen om een persoon te verwijderen zouden we de volgende foutmelding krijgen:
```
The DELETE statement conflicted with the REFERENCE constraint "GroupMembers_FK_persons". The conflict occurred in database "...\DBLINQ.MDF", table "dbo.tblGroupMembers", column 'PER_ID'.

```

Hetzelfde geldt voor groepen verwijderen. Om dit op te lossen gaan we eerst alle groupmembers moeten verwijderen.

```

	public static void RemovePerson(Person person)
	{
		DataRow row;
		try
		{
			List<GroupMember> listOfGroups = GetAllGroupsOfPerson(person);
			RemoveGroupMembers(listOfGroups);

			row = DataSet.Tables["tblPersons"]
				.AsEnumerable()
				.FirstOrDefault(dataRow => dataRow.Field<int>("Id") == person.Id);

			row?.Delete();
		}
		catch (Exception ex)
		{
			Console.WriteLine("ERROR RemovePerson" + ex.Message);
		}
		finally
		{
			SaveChangesToDb();
		}
	}
	public static void RemoveGroup(Group group)
	{
		DataRow row;
		try
		{
			RemoveGroupMembers(group.AllGroupMembers);

			row = DataSet.Tables["tblGroups"]
				.AsEnumerable()
				.FirstOrDefault(dataRow => dataRow.Field<int>("Id") == group.Id);

			row?.Delete();
		}
		catch (Exception ex)
		{
			Console.WriteLine("ERROR EditGroup" + ex.Message);
		}
		finally
		{
			SaveChangesToDb();
		}
	}
	
	public static List<GroupMember> GetAllGroupsOfPerson(Person person)
	{
		List<GroupMember> lst = new List<GroupMember>();

		DataRow personDataRow = DataSet.Tables["tblPersons"]
			.AsEnumerable()
			.FirstOrDefault(g => g.Field<int>("Id") == person.Id);

		DataRow[] allGroups = personDataRow?.GetChildRows(RelationGroupMemberToPerson);

		lst = allGroups?.Select(gmRow => new GroupMember(
				person,
				GetGroupById(gmRow.Field<int>("GRP_ID"))
			)).ToList();

		return lst;
	}
	
	private static void RemoveGroupMember(GroupMember groupMember)
	{
		DataRow row;
		try
		{
			row = DataSet.Tables["tblGroupMembers"]
				.AsEnumerable()
				.FirstOrDefault(dataRow => dataRow.Field<int>("PER_ID") == groupMember.Person.Id
										   && dataRow.Field<int>("GRP_ID") == groupMember.Group.Id
				 );

			row?.Delete();
		}
		catch (Exception ex)
		{
			Console.WriteLine("ERROR RemoveGroupMember" + ex.Message);
		}
		finally
		{
			SaveChangesToDb();
		}
	}
	
	private static void RemoveGroupMembers(List<GroupMember> list)
	{
		list.ForEach(RemoveGroupMember);
	}

```

### Testen
Test in de methode *Main* van *Program* alle CRUD operaties:
```

	static void Main(string[] args)
	{
		Person testPerson1 = new Person(-1, "John", "Bon Jovi", "jbj@mail.com");
		Person testPerson2 = new Person(-1, "Ela", "Fitzgerald", "af@mail.com");
		Person testPerson3 = new Person(-1, "Billie", "Holiday", "bh@mail.com");
		Person testPerson4 = new Person(-1, "Etta", "James", "ej@mail.com");
		Group testGroup1 = new Group(-1, "Rock");
		Group testGroup2 = new Group(-1, "Jazz");


		Console.WriteLine("\n=========================ADD PERSON=========================\n");
		
		Console.WriteLine($"#Persons: {DbAccess.GetAllPersons().Count}");

		DbAccess.AddPerson(testPerson1);
		DbAccess.AddPerson(testPerson2);
		DbAccess.AddPerson(testPerson3);
		DbAccess.AddPerson(testPerson4);

		testPerson1 = DbAccess.GetAllPersons()
							  .FirstOrDefault(p => p.Email == testPerson1.Email);
		testPerson2 = DbAccess.GetAllPersons()
							  .FirstOrDefault(p => p.Email == testPerson2.Email);
		testPerson3 = DbAccess.GetAllPersons()
							  .FirstOrDefault(p => p.Email == testPerson3.Email);
		testPerson4 = DbAccess.GetAllPersons()
							  .FirstOrDefault(p => p.Email == testPerson4.Email);
		
		Console.WriteLine($"#Persons: {DbAccess.GetAllPersons().Count}");

		foreach (Person person in DbAccess.GetAllPersons())
		{
			Console.WriteLine($"\t\t{person}");
		}

		Console.WriteLine("\n========================REMOVE PERSON=======================\n");
		DbAccess.RemovePerson(testPerson1);

		Console.WriteLine($"#Persons: {DbAccess.GetAllPersons().Count}");
		foreach (Person person in DbAccess.GetAllPersons())
		{
			Console.WriteLine($"\t\t{person}");
		}

		Console.WriteLine("\n========================EDIT PERSON=========================\n");
		testPerson2.FirstName = "Ella";
		DbAccess.EditPerson(testPerson2);

		Console.WriteLine($"#Persons: {DbAccess.GetAllPersons().Count}");
		foreach (Person person in DbAccess.GetAllPersons())
		{
			Console.WriteLine($"\t\t{person}");
		}
		
		Console.WriteLine("\n=========================ADD GROUP==========================\n");
		DbAccess.AddGroup(testGroup1);
		DbAccess.AddGroup(testGroup2);

		testGroup1 = DbAccess.GetAllGroups()
			.FirstOrDefault(g => g.Title == testGroup1.Title);
		testGroup2 = DbAccess.GetAllGroups()
			.FirstOrDefault(g => g.Title == testGroup2.Title);

		Console.WriteLine($"#Groups: {DbAccess.GetAllGroups().Count}");
		foreach (Group group in DbAccess.GetAllGroups())
		{
			Console.WriteLine($"\t\t{group}");
		}

		Console.WriteLine("\n=========================EDIT GROUP=========================\n");
		testGroup1.Title = "Singers";
		DbAccess.EditGroup(testGroup1);

		Console.WriteLine($"#Groups: {DbAccess.GetAllGroups().Count}");
		foreach (Group group in DbAccess.GetAllGroups())
		{
			Console.WriteLine($"\t\t{group}");
		}

		Console.WriteLine("\n========================REMOVE GROUP========================\n");
		DbAccess.RemoveGroup(testGroup1);

		Console.WriteLine($"#Groups: {DbAccess.GetAllGroups().Count}");
		foreach (Group group in DbAccess.GetAllGroups())
		{
			Console.WriteLine($"\t\t{group}");
		}
		
		Console.WriteLine("\n======================ADD GROUPMEMBERS======================\n");
		DbAccess.AddPersonToGroup(testPerson2, testGroup2);
		DbAccess.AddPersonToGroup(testPerson3, testGroup2);
		DbAccess.AddPersonToGroup(testPerson4, testGroup2);
		testGroup2 = DbAccess.GetAllGroups()
			.FirstOrDefault(g => g.Title == testGroup2.Title);
		
		Console.WriteLine($"#Groups: {DbAccess.GetAllGroups().Count}");
		foreach (Group group in DbAccess.GetAllGroups())
		{
			Console.WriteLine($"\t\t{group}");
		}
		
		Console.WriteLine($"#GroupsMembers: {DbAccess.GetAllGroupMembers().Count}");
		Console.WriteLine("\n====================REMOVE GROUPMEMBERS======================\n");

		DbAccess.RemovePersonFromGroup(testPerson4, testGroup2);

		Console.WriteLine($"#Groups: {DbAccess.GetAllGroups().Count}");
		foreach (Group group in DbAccess.GetAllGroups())
		{
			Console.WriteLine($"\t\t{group}");
		}
		Console.WriteLine($"#GroupsMembers: {DbAccess.GetAllGroupMembers().Count}");

		Console.WriteLine("\n====================Foreign Key Constraints=====================\n");

		Console.WriteLine($"Removing {testPerson2.FirstName}");
		DbAccess.RemovePerson(testPerson2);
		Console.WriteLine($"Removing {testGroup2.Title}");
		DbAccess.RemoveGroup(testGroup2);
		Console.WriteLine();

		Console.WriteLine($"#Persons: {DbAccess.GetAllPersons().Count}");
		foreach (Person person in DbAccess.GetAllPersons())
		{
			Console.WriteLine($"\t\t{person}");
		}

		Console.WriteLine($"#Groups: {DbAccess.GetAllGroups().Count}");
		foreach (Group group in DbAccess.GetAllGroups())
		{
			Console.WriteLine($"\t\t{group}");
		}

		Console.WriteLine($"#GroupsMembers: {DbAccess.GetAllGroupMembers().Count}");
		
		Console.ReadKey();

	}

```
Als alles juist geïmplementeerd werd, krijgt je volgende output te zien:
```

	=========================ADD PERSON=========================

	#Persons: 0
	#Persons: 4
					[1] John, Bon Jovi
					[2] Ela, Fitzgerald
					[3] Billie, Holiday
					[4] Etta, James

	========================REMOVE PERSON=======================

	#Persons: 3
					[2] Ela, Fitzgerald
					[3] Billie, Holiday
					[4] Etta, James

	========================EDIT PERSON=========================

	#Persons: 3
					[2] Ella, Fitzgerald
					[3] Billie, Holiday
					[4] Etta, James

	=========================ADD GROUP==========================

	#Groups: 2
					[2] Jazz - #:0
					[1] Rock - #:0

	=========================EDIT GROUP=========================

	#Groups: 2
					[2] Jazz - #:0
					[1] Singers - #:0

	========================REMOVE GROUP========================

	#Groups: 1
					[2] Jazz - #:0

	======================ADD GROUPMEMBERS======================

	#Groups: 1
					[2] Jazz - #:3
							> Ella Fitzgerald
							> Billie Holiday
							> Etta James

	#GroupsMembers: 3

	====================REMOVE GROUPMEMBERS======================

	#Groups: 1
					[2] Jazz - #:2
							> Ella Fitzgerald
							> Billie Holiday

	#GroupsMembers: 2

	====================Foreign Key Constraints=====================

	Removing Ella
	Removing Jazz

	#Persons: 2
					[3] Billie, Holiday
					[4] Etta, James
	#Groups: 0
	#GroupsMembers: 0
```
### Extensies

Het is mogelijk om via *Extensions* en *Generic functions* en Datatable om te zetten naar een lijst. Dit zou je nog kunnen toevoegen.

[Stack Overflow: Convert DataSet to List](http://stackoverflow.com/questions/17107220/convert-dataset-to-list#answer-17107297)

