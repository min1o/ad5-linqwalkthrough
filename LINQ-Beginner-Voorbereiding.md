# Dataset querying met LINQ
**Author:** Ingabire Marie-Aimée
**Course:**  AD5

## Vereisten
De walkthrough is gemaakt en getest op Visual Studio 2015 (Ultimate edition). Het kan zijn dat sommige operaties niet werken op andere versies.


## Voorbereiding

Om te beginnen zullen we een database moeten opstellen. Voor deze walkthrough hebben zullen we ons houden aan locale databanken.
Vooraleer we hieraan kunnen beginnen moeten we eerste een project aanmaken.

Ga naar `File > New > Project`. Kies dan voor een **Console Application**, noem het *DatasetsMetLINQ*. De naam maakt opzich niet uit.

### Locale database aanmaken

Visual studio laat u toe om locale databanken aan te maken. In deze walkthrough zullen we gebruiken maken van een *MS SQL Server Database File*. De locale databanken hebben een `.mdf` extensie en hebben T-SQL als querytaal.

Selectee in *Solution Explorer* zijbalk uw project.
Ga nu naar `Project > Add New Data Source...`.
Kies voor de optie *Database* klik op 'Next' en kies voor *Dataset*.
Klik nu op 'New Connection'.

Klik op 'Browse' en kies de locatie waar de locale database moet bewaard worden. Het is aangeraden om het te bewaren in uw projectbestand. Geef het de naam *DBLinq*. Klik dan op 'OK'.
Visual studio zal u dan de vraag stellen of u 'DBLinq.mdf' wil aanmaken, kies 'Yes'.

U scherm zou er dan als volgt moeten uitzien. Kies 'Next' om verder te gaan. Kies 'Yes' zodat de DBLinq.mdf gekopieerd wordt naar uw project directory en het zichbaar is in uw *Solution Explorer*.
Kies 'Yes' als u gevraagd wordt of u de huidige 'DBLinq.mdf' wil overschrijven.

Verander de ConnectionString naam naar 'DBLinqCS'. Dit is verandert niets aan de applicatie maar is gewoon korter :).
Klik op 'Next' en dan op 'Finish'.

Nu zou u normaal gezien in uw `DatasetsMetLINQ > Properties > Settings` een nieuwe setting moeten hebben met de ConnectionString naar de locale database. En uw `App.config` is er nu een connectionString-tag aanwezig.

### Stuctuur aanmaken

Vooraleer je met de database gaan kunnen interageren moet je eerst de structuur van de database aanmaken. Dit kan via de *Server Explorer* van Visual Studio.

Druk `CTRL + W, L` of ga naar `View > Server Explorer`.
Rechtsklik op 'DBLinq.mdf' en kies dan voor 'Refresh'.

#### Structuur

Rechtsklik op 'DBLinq.mdf' en kies nu voor 'New Query'. Kopieer wat hieronder is in de editor, klik dan op 'Execute'.
Als je nu opniew een refresh doe van de databank zou je 2 tabellen moeten hebben.

Het script zal de tabellen `tblPersons` en `tblGroups`. Dit is een simpele structuur, een groep heeft nul of meedere gebruikers en een gebruiker behoort altijd tot een group.

