	/*
	* Author: Ingabire Marie-Aimée
	* Course: AD5
	* AJ: 2015-2016
	*/

	/*
	* ----------------------------------------- TABLES
	*/
	if not exists (select name from sys.tables where name = 'tblPersons')
	begin
		CREATE TABLE tblPersons
		(
			Id				INT NOT NULL			IDENTITY(1,1),
			FirstName	NVARCHAR(50) NOT NULL,
			LastName	NVARCHAR(50) NOT NULL,
			Email		NVARCHAR(50) NOT NULL
		)
	end
	go
	if not exists (select name from sys.tables where name = 'tblGroupMembers')
	begin
		CREATE TABLE tblGroupMembers
		(
			PER_ID		INT NOT NULL,
			GRP_ID		INT NOT NULL
		)
	end
	go
	if not exists (select name from sys.tables where name = 'tblGroups')
	begin
		CREATE TABLE tblGroups
		(
			Id			INT NOT NULL			IDENTITY(1,1),
			Title		NVARCHAR(50) NOT NULL
		)
	end
	go

	/*
	* ----------------------------------------- CONSTRAINTS
	*/

	if not exists (select CONSTRAINT_NAME from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where CONSTRAINT_NAME = 'Groups_PK')
	begin
		ALTER TABLE tblGroups
			ADD
				CONSTRAINT Groups_PK				PRIMARY KEY(Id),
				CONSTRAINT Groups_UQ_title			UNIQUE(Title)
	end
	go

	if not exists (select CONSTRAINT_NAME from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where CONSTRAINT_NAME = 'Persons_PK')
	begin
		ALTER TABLE tblPersons
			ADD
				CONSTRAINT Persons_PK				PRIMARY KEY(Id),
				CONSTRAINT Persons_UQ_email			UNIQUE(Email)
	end
	go

	if not exists (select CONSTRAINT_NAME from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where CONSTRAINT_NAME = 'GroupMembers_PK' OR CONSTRAINT_NAME = 'GroupMembers_FK_persons' OR CONSTRAINT_NAME = 'GroupMembers_FK_groups')
	begin
		ALTER TABLE tblGroupMembers
			ADD
				CONSTRAINT GroupMembers_PK			PRIMARY KEY(PER_ID, GRP_ID),
				CONSTRAINT GroupMembers_FK_persons	FOREIGN KEY(PER_ID) REFERENCES tblPersons(Id),
				CONSTRAINT GroupMembers_FK_groups	FOREIGN KEY(GRP_ID) REFERENCES tblGroups(Id)
	end
	go
