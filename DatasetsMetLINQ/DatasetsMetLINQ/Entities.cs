﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatasetsMetLINQ
{
    public class Person
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }

        public Person():this(-1, String.Empty, String.Empty, String.Empty)
        {}
        public Person(int id, String firstname, String lastname, String email)
        {
            Id = id;
            FirstName = firstname;
            LastName = lastname;
            Email = email;
        }

        public override string ToString()
        {
            return $"[{Id}] {FirstName}, {LastName}";
        }
    }
    public class GroupMember
    {
        public Person Person { get; set; }
        public Group Group { get; set; }

        public GroupMember(): this(new Person(), new Group())
        {}
        public GroupMember(Person person, Group group)
        {
            Person = person;
            Group = group;
        }

    }
    public class Group
    {
        public int Id { get; set; }
        public String Title { get; set; }
        public List<GroupMember> AllGroupMembers { get; set; }

        public Group(): this(-1, String.Empty)
        {}
        public Group(int id, String title)
        {
            Id = id;
            Title = title;
        }

        public override string ToString()
        {
            int count = AllGroupMembers?.Count ?? 0;

            String msg = $"[{Id}] {Title} - #:{count}";

            if (count > 0)
            {
                msg += "\n\t\t\t";
                AllGroupMembers?
                    .ForEach(gm => msg += $"> {gm.Person.FirstName} {gm.Person.LastName}" + "\n\t\t\t");
            }
            
            return msg;
        }
    }
}
