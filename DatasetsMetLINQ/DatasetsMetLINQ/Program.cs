﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatasetsMetLINQ
{
    class Program
    {

        static void Main(string[] args)
        {
            //Test();
            TestWithExtensions();
            Console.ReadKey();

        }
        static void Test()
        {
            Person testPerson1 = new Person(-1, "John", "Bon Jovi", "jbj@mail.com");
            Person testPerson2 = new Person(-1, "Ela", "Fitzgerald", "af@mail.com");
            Person testPerson3 = new Person(-1, "Billie", "Holiday", "bh@mail.com");
            Person testPerson4 = new Person(-1, "Etta", "James", "ej@mail.com");
            Group testGroup1 = new Group(-1, "Rock");
            Group testGroup2 = new Group(-1, "Jazz");


            Console.WriteLine("\n=========================ADD PERSON=========================\n");

            Console.WriteLine($"#Persons: {DbAccess.GetAllPersons().Count}");

            DbAccess.AddPerson(testPerson1);
            DbAccess.AddPerson(testPerson2);
            DbAccess.AddPerson(testPerson3);
            DbAccess.AddPerson(testPerson4);

            testPerson1 = DbAccess.GetAllPersons()
                                  .FirstOrDefault(p => p.Email == testPerson1.Email);
            testPerson2 = DbAccess.GetAllPersons()
                                      .FirstOrDefault(p => p.Email == testPerson2.Email);
            testPerson3 = DbAccess.GetAllPersons()
                                      .FirstOrDefault(p => p.Email == testPerson3.Email);
            testPerson4 = DbAccess.GetAllPersons()
                                      .FirstOrDefault(p => p.Email == testPerson4.Email);

            Console.WriteLine($"#Persons: {DbAccess.GetAllPersons().Count}");

            foreach (Person person in DbAccess.GetAllPersons())
            {
                Console.WriteLine($"\t\t{person}");
            }

            Console.WriteLine("\n========================REMOVE PERSON=======================\n");
            DbAccess.RemovePerson(testPerson1);

            Console.WriteLine($"#Persons: {DbAccess.GetAllPersons().Count}");
            foreach (Person person in DbAccess.GetAllPersons())
            {
                Console.WriteLine($"\t\t{person}");
            }

            Console.WriteLine("\n========================EDIT PERSON=========================\n");
            testPerson2.FirstName = "Ella";
            DbAccess.EditPerson(testPerson2);

            Console.WriteLine($"#Persons: {DbAccess.GetAllPersons().Count}");
            foreach (Person person in DbAccess.GetAllPersons())
            {
                Console.WriteLine($"\t\t{person}");
            }

            Console.WriteLine("\n=========================ADD GROUP==========================\n");
            DbAccess.AddGroup(testGroup1);
            DbAccess.AddGroup(testGroup2);

            testGroup1 = DbAccess.GetAllGroups()
                .FirstOrDefault(g => g.Title == testGroup1.Title);
            testGroup2 = DbAccess.GetAllGroups()
                            .FirstOrDefault(g => g.Title == testGroup2.Title);

            Console.WriteLine($"#Groups: {DbAccess.GetAllGroups().Count}");
            foreach (Group group in DbAccess.GetAllGroups())
            {
                Console.WriteLine($"\t\t{group}");
            }

            Console.WriteLine("\n=========================EDIT GROUP=========================\n");
            testGroup1.Title = "Singers";
            DbAccess.EditGroup(testGroup1);

            Console.WriteLine($"#Groups: {DbAccess.GetAllGroups().Count}");
            foreach (Group group in DbAccess.GetAllGroups())
            {
                Console.WriteLine($"\t\t{group}");
            }

            Console.WriteLine("\n========================REMOVE GROUP========================\n");
            DbAccess.RemoveGroup(testGroup1);

            Console.WriteLine($"#Groups: {DbAccess.GetAllGroups().Count}");
            foreach (Group group in DbAccess.GetAllGroups())
            {
                Console.WriteLine($"\t\t{group}");
            }

            Console.WriteLine("\n======================ADD GROUPMEMBERS======================\n");
            DbAccess.AddPersonToGroup(testPerson2, testGroup2);
            DbAccess.AddPersonToGroup(testPerson3, testGroup2);
            DbAccess.AddPersonToGroup(testPerson4, testGroup2);
            testGroup2 = DbAccess.GetAllGroups()
                .FirstOrDefault(g => g.Title == testGroup2.Title);

            Console.WriteLine($"#Groups: {DbAccess.GetAllGroups().Count}");
            foreach (Group group in DbAccess.GetAllGroups())
            {
                Console.WriteLine($"\t\t{group}");
            }

            Console.WriteLine($"#GroupsMembers: {DbAccess.GetAllGroupMembers().Count}");
            Console.WriteLine("\n====================REMOVE GROUPMEMBERS======================\n");

            DbAccess.RemovePersonFromGroup(testPerson4, testGroup2);

            Console.WriteLine($"#Groups: {DbAccess.GetAllGroups().Count}");
            foreach (Group group in DbAccess.GetAllGroups())
            {
                Console.WriteLine($"\t\t{group}");
            }
            Console.WriteLine($"#GroupsMembers: {DbAccess.GetAllGroupMembers().Count}");

            Console.WriteLine("\n====================Foreign Key Constraints=====================\n");

            Console.WriteLine($"Removing {testPerson2.FirstName}");
            DbAccess.RemovePerson(testPerson2);
            Console.WriteLine($"Removing {testGroup2.Title}");
            DbAccess.RemoveGroup(testGroup2);
            Console.WriteLine();

            Console.WriteLine($"#Persons: {DbAccess.GetAllPersons().Count}");
            foreach (Person person in DbAccess.GetAllPersons())
            {
                Console.WriteLine($"\t\t{person}");
            }

            Console.WriteLine($"#Groups: {DbAccess.GetAllGroups().Count}");
            foreach (Group group in DbAccess.GetAllGroups())
            {
                Console.WriteLine($"\t\t{group}");
            }

            Console.WriteLine($"#GroupsMembers: {DbAccess.GetAllGroupMembers().Count}");

        }
        static void TestWithExtensions()
        {

            Person testPerson1 = new Person(-1, "John", "Bon Jovi", "jbj@mail.com");
            Person testPerson2 = new Person(-1, "Ela", "Fitzgerald", "af@mail.com");
            Person testPerson3 = new Person(-1, "Billie", "Holiday", "bh@mail.com");
            Person testPerson4 = new Person(-1, "Etta", "James", "ej@mail.com");
            Group testGroup1 = new Group(-1, "Rock");
            Group testGroup2 = new Group(-1, "Jazz");


            Console.WriteLine("\n=========================ADD PERSON=========================\n");

            Console.WriteLine($"#Persons: {DbAccessWithEx.GetAllPersons().Count}");

            DbAccessWithEx.AddPerson(testPerson1);
            DbAccessWithEx.AddPerson(testPerson2);
            DbAccessWithEx.AddPerson(testPerson3);
            DbAccessWithEx.AddPerson(testPerson4);

            testPerson1 = DbAccessWithEx.GetAllPersons()
                                  .FirstOrDefault(p => p.Email == testPerson1.Email);
            testPerson2 = DbAccessWithEx.GetAllPersons()
                                  .FirstOrDefault(p => p.Email == testPerson2.Email);
            testPerson3 = DbAccessWithEx.GetAllPersons()
                                  .FirstOrDefault(p => p.Email == testPerson3.Email);
            testPerson4 = DbAccessWithEx.GetAllPersons()
                                  .FirstOrDefault(p => p.Email == testPerson4.Email);

            Console.WriteLine($"#Persons: {DbAccessWithEx.GetAllPersons().Count}");

            foreach (Person person in DbAccessWithEx.GetAllPersons())
            {
                Console.WriteLine($"\t\t{person}");
            }

            Console.WriteLine("\n========================REMOVE PERSON=======================\n");
            DbAccessWithEx.RemovePerson(testPerson1);

            Console.WriteLine($"#Persons: {DbAccessWithEx.GetAllPersons().Count}");
            foreach (Person person in DbAccessWithEx.GetAllPersons())
            {
                Console.WriteLine($"\t\t{person}");
            }

            Console.WriteLine("\n========================EDIT PERSON=========================\n");
            testPerson2.FirstName = "Ella";
            DbAccessWithEx.EditPerson(testPerson2);

            Console.WriteLine($"#Persons: {DbAccessWithEx.GetAllPersons().Count}");
            foreach (Person person in DbAccessWithEx.GetAllPersons())
            {
                Console.WriteLine($"\t\t{person}");
            }

            Console.WriteLine("\n=========================ADD GROUP==========================\n");
            DbAccessWithEx.AddGroup(testGroup1);
            DbAccessWithEx.AddGroup(testGroup2);

            testGroup1 = DbAccessWithEx.GetAllGroups()
                .FirstOrDefault(g => g.Title == testGroup1.Title);
            testGroup2 = DbAccessWithEx.GetAllGroups()
                .FirstOrDefault(g => g.Title == testGroup2.Title);

            Console.WriteLine($"#Groups: {DbAccessWithEx.GetAllGroups().Count}");
            foreach (Group group in DbAccessWithEx.GetAllGroups())
            {
                Console.WriteLine($"\t\t{group}");
            }

            Console.WriteLine("\n=========================EDIT GROUP=========================\n");
            testGroup1.Title = "Singers";
            DbAccessWithEx.EditGroup(testGroup1);

            Console.WriteLine($"#Groups: {DbAccessWithEx.GetAllGroups().Count}");
            foreach (Group group in DbAccessWithEx.GetAllGroups())
            {
                Console.WriteLine($"\t\t{group}");
            }

            Console.WriteLine("\n========================REMOVE GROUP========================\n");
            DbAccessWithEx.RemoveGroup(testGroup1);

            Console.WriteLine($"#Groups: {DbAccessWithEx.GetAllGroups().Count}");
            foreach (Group group in DbAccessWithEx.GetAllGroups())
            {
                Console.WriteLine($"\t\t{group}");
            }

            Console.WriteLine("\n======================ADD GROUPMEMBERS======================\n");
            DbAccessWithEx.AddPersonToGroup(testPerson2, testGroup2);
            DbAccessWithEx.AddPersonToGroup(testPerson3, testGroup2);
            DbAccessWithEx.AddPersonToGroup(testPerson4, testGroup2);
            testGroup2 = DbAccessWithEx.GetAllGroups()
                .FirstOrDefault(g => g.Title == testGroup2.Title);

            Console.WriteLine($"#Groups: {DbAccessWithEx.GetAllGroups().Count}");
            foreach (Group group in DbAccessWithEx.GetAllGroups())
            {
                Console.WriteLine($"\t\t{group}");
            }

            Console.WriteLine($"#GroupsMembers: {DbAccessWithEx.GetAllGroupMembers().Count}");
            Console.WriteLine("\n====================REMOVE GROUPMEMBERS======================\n");

            DbAccessWithEx.RemovePersonFromGroup(testPerson4, testGroup2);

            Console.WriteLine($"#Groups: {DbAccessWithEx.GetAllGroups().Count}");
            foreach (Group group in DbAccessWithEx.GetAllGroups())
            {
                Console.WriteLine($"\t\t{group}");
            }
            Console.WriteLine($"#GroupsMembers: {DbAccessWithEx.GetAllGroupMembers().Count}");

            Console.WriteLine("\n====================Foreign Key Constraints=====================\n");

            Console.WriteLine($"Removing {testPerson2.FirstName}");
            DbAccessWithEx.RemovePerson(testPerson2);
            Console.WriteLine($"Removing {testGroup2.Title}");
            DbAccessWithEx.RemoveGroup(testGroup2);
            Console.WriteLine();

            Console.WriteLine($"#Persons: {DbAccessWithEx.GetAllPersons().Count}");
            foreach (Person person in DbAccessWithEx.GetAllPersons())
            {
                Console.WriteLine($"\t\t{person}");
            }

            Console.WriteLine($"#Groups: {DbAccessWithEx.GetAllGroups().Count}");
            foreach (Group group in DbAccessWithEx.GetAllGroups())
            {
                Console.WriteLine($"\t\t{group}");
            }

            Console.WriteLine($"#GroupsMembers: {DbAccessWithEx.GetAllGroupMembers().Count}");
            

        }
    }
}
