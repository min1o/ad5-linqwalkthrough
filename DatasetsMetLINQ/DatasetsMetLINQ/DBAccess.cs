﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatasetsMetLINQ
{
    public class DbAccess
    {
        private static readonly SqlConnection _DbConnection;
        private static SqlDataAdapter _PersonsAdapter;
        private static SqlDataAdapter _GroupMembersAdapter;
        private static SqlDataAdapter _GroupsAdapter;
        private static DataSet DataSet { get; set; }
        public static DataRelation RelationGroupMemberToPerson { get; set; }
        public static DataRelation RelationGroupMemberToGroup { get; set; }

        static DbAccess()
        {
            String connectionString = Properties.Settings.Default.DBLinqCS ?? "";
            _DbConnection = new SqlConnection(connectionString);

            if (!string.IsNullOrEmpty(_DbConnection.ConnectionString))
            {
                InitDatasets();
            }
            else
            {
                Console.WriteLine("Error could not connect to the database.");
            }
        }

        private static void InitDatasets()
        {
            SqlCommandBuilder cmdBuilderNot;
            // ------------------- DATASETS
            DataSet = new DataSet("AppDataset");

            // ------------------- ADAPTERS
            _PersonsAdapter = new SqlDataAdapter("SELECT * FROM tblPersons", _DbConnection);
            _PersonsAdapter.TableMappings.Add("Table", "tblPersons");
            _PersonsAdapter.Fill(DataSet);

            _GroupMembersAdapter = new SqlDataAdapter("SELECT * FROM tblGroupMembers", _DbConnection);
            _GroupMembersAdapter.TableMappings.Add("Table", "tblGroupMembers");
            _GroupMembersAdapter.Fill(DataSet);

            _GroupsAdapter = new SqlDataAdapter("SELECT * FROM tblGroups", _DbConnection);
            _GroupsAdapter.TableMappings.Add("Table", "tblGroups");
            _GroupsAdapter.Fill(DataSet);


            // ------------------- SETUP COMMANDS
            cmdBuilderNot = new SqlCommandBuilder(_PersonsAdapter);
            _PersonsAdapter.InsertCommand = cmdBuilderNot.GetInsertCommand();
            _PersonsAdapter.DeleteCommand = cmdBuilderNot.GetDeleteCommand();
            _PersonsAdapter.UpdateCommand = cmdBuilderNot.GetUpdateCommand();

            cmdBuilderNot = new SqlCommandBuilder(_GroupMembersAdapter);
            _GroupMembersAdapter.InsertCommand = cmdBuilderNot.GetInsertCommand();
            _GroupMembersAdapter.DeleteCommand = cmdBuilderNot.GetDeleteCommand();
            _GroupMembersAdapter.UpdateCommand = cmdBuilderNot.GetUpdateCommand();

            cmdBuilderNot = new SqlCommandBuilder(_GroupsAdapter);
            _GroupsAdapter.InsertCommand = cmdBuilderNot.GetInsertCommand();
            _GroupsAdapter.DeleteCommand = cmdBuilderNot.GetDeleteCommand();
            _GroupsAdapter.UpdateCommand = cmdBuilderNot.GetUpdateCommand();

            // ------------------- RELATIONS

            DataColumn colParent;
            DataColumn colChild;
            colParent = DataSet.Tables["tblGroups"].Columns["Id"];
            colChild = DataSet.Tables["tblGroupMembers"].Columns["GRP_ID"];
            RelationGroupMemberToGroup = new DataRelation("RelationGroupMemberToGroup", colParent, colChild);
            DataSet.Relations.Add(RelationGroupMemberToGroup);

            colParent = DataSet.Tables["tblPersons"].Columns["Id"];
            colChild = DataSet.Tables["tblGroupMembers"].Columns["PER_ID"];
            RelationGroupMemberToPerson = new DataRelation("RelationGroupMemberToPerson", colParent, colChild);
            DataSet.Relations.Add(RelationGroupMemberToPerson);
        }


        private static void Connect()
        {
            if (_DbConnection.State == ConnectionState.Closed)
            {
                _DbConnection.Open();
            }
        }
        private static void Disconnect()
        {
            if (_DbConnection.State != ConnectionState.Closed)
            {
                _DbConnection.Close();
            }
        }
        private static void SaveChangesToDb()
        {

            try
            {
                Connect();

                int rslt;

                rslt = _PersonsAdapter.Update(DataSet, "tblPersons");
                Debug.WriteLine("\n\nDB>tblPersons -- # changed:" + rslt);

                rslt = _GroupsAdapter.Update(DataSet, "tblGroups");
                Debug.WriteLine("\n\nDB>tblGroups -- # changed:" + rslt);

                rslt = _GroupMembersAdapter.Update(DataSet, "tblGroupMembers");
                Debug.WriteLine("\n\nDB>tblGroupMembers -- # changed:" + rslt);

                Disconnect();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, "ERROR SaveChanges");
            }
            finally
            {
                UpdateDataset();
            }
        }
        private static void UpdateDataset()
        {
            Connect();

            DataSet.AcceptChanges();
            DataSet.Clear();
            DataSet.EnforceConstraints = false;
            _PersonsAdapter.Fill(DataSet);
            _GroupsAdapter.Fill(DataSet);
            _GroupMembersAdapter.Fill(DataSet);
            DataSet.EnforceConstraints = true;

            Disconnect();
        }

        public static List<Person> GetAllPersons()
        {
            List<Person> lstPersons = new List<Person>();

            lstPersons = DataSet.Tables["tblPersons"]
                .AsEnumerable()
                .Select(dataRow => new Person(
                    dataRow.Field<int>("Id"), 
                    dataRow.Field<String>("FirstName"), 
                    dataRow.Field<String>("LastName"), 
                    dataRow.Field<String>("Email")))
                .ToList();
            return lstPersons;
        }
        private static Person GetPersonById(int id)
        {
            return GetAllPersons().FirstOrDefault(p => p.Id == id);
        }
        public static void AddPerson(Person person)
        {
            try
            {
                DataRow newRow = DataSet.Tables["tblPersons"].NewRow();

                newRow["FirstName"] = person.FirstName;
                newRow["LastName"] = person.LastName;
                newRow["Email"] = person.Email;

                DataSet.Tables["tblPersons"].Rows.Add(newRow);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR AddPerson" + ex.Message);
            }
            finally
            {
                SaveChangesToDb();
            }
        }
        public static void RemovePerson(Person person)
        {
            DataRow row;
            try
            {
                List<GroupMember> listOfGroups = GetAllGroupsOfPerson(person);
                RemoveGroupMembers(listOfGroups);

                row = DataSet.Tables["tblPersons"]
                    .AsEnumerable()
                    .FirstOrDefault(dataRow => dataRow.Field<int>("Id") == person.Id);

                row?.Delete();
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR RemovePerson" + ex.Message);
            }
            finally
            {
                SaveChangesToDb();
            }
        }

        public static void EditPerson(Person person)
        {
            DataRow row;
            try
            {
                row = DataSet.Tables["tblPersons"]
                    .AsEnumerable()
                    .FirstOrDefault(dataRow => dataRow.Field<String>("Email") == person.Email);
                
                row?.SetField<String>("FirstName", person.FirstName);
                row?.SetField<String>("LastName", person.LastName);
                row?.SetField<String>("Email", person.Email);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR EditPerson" + ex.Message);
            }
            finally
            {
                SaveChangesToDb();
            }
        }

        public static List<Group> GetAllGroups()
        {
            List<Group> lstGroups = new List<Group>();

            lstGroups = DataSet.Tables["tblGroups"]
                .AsEnumerable()
                .Select(dataRow => new Group(
                    dataRow.Field<int>("Id"),
                    dataRow.Field<String>("Title")
                 ))
                .ToList();

            lstGroups
                .ForEach(g => g.AllGroupMembers = GetAllMembersInGroup(g));

            return lstGroups;
        }
        private static Group GetGroupById(int id)
        {
            return GetAllGroups().FirstOrDefault(g => g.Id == id);
        }
        public static void AddGroup(Group testGroup)
        {
            try
            {
                DataRow newRow = DataSet.Tables["tblGroups"].NewRow();

                newRow["Title"] = testGroup.Title;

                DataSet.Tables["tblGroups"].Rows.Add(newRow);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR AddGroup" + ex.Message);
            }
            finally
            {
                SaveChangesToDb();
            }
        }
        public static void EditGroup(Group group)
        {
            DataRow row;
            try
            {
                row = DataSet.Tables["tblGroups"]
                    .AsEnumerable()
                    .FirstOrDefault(dataRow => dataRow.Field<int>("Id") == group.Id);

                row?.SetField<String>("Title", group.Title);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR EditGroup" + ex.Message);
            }
            finally
            {
                SaveChangesToDb();
            }
        }
        public static void RemoveGroup(Group group)
        {
            DataRow row;
            try
            {
                RemoveGroupMembers(group.AllGroupMembers);

                row = DataSet.Tables["tblGroups"]
                    .AsEnumerable()
                    .FirstOrDefault(dataRow => dataRow.Field<int>("Id") == group.Id);

                row?.Delete();
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR EditGroup" + ex.Message);
            }
            finally
            {
                SaveChangesToDb();
            }
        }
        public static void AddPersonToGroup(Person person, Group group)
        {
            try
            {
                DataRow newRow = DataSet.Tables["tblGroupMembers"].NewRow();

                newRow["PER_ID"] = person.Id;
                newRow["GRP_ID"] = group.Id;

                DataSet.Tables["tblGroupMembers"].Rows.Add(newRow);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR AddPersonToGroup" + ex.Message);
            }
            finally
            {
                SaveChangesToDb();
            }

        }
        public static void RemovePersonFromGroup(Person person, Group group)
        {
            DataRow row;
            try
            {
                row = DataSet.Tables["tblGroupMembers"]
                    .AsEnumerable()
                    .FirstOrDefault( dataRow => dataRow.Field<int>("PER_ID") == person.Id 
                                                && dataRow.Field<int>("GRP_ID") == group.Id
                     );

                row?.Delete();
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR RemovePerson" + ex.Message);
            }
            finally
            {
                SaveChangesToDb();
            }
        }

        public static List<GroupMember> GetAllGroupMembers()
        {
            List<GroupMember> lstGroupMembers = new List<GroupMember>();

            lstGroupMembers = DataSet.Tables["tblGroupMembers"]
                .AsEnumerable()
                .Select(row => new GroupMember(
                    GetPersonById(row.Field<int>("PER_ID")),
                    GetGroupById(row.Field<int>("GRP_ID"))
                    )
                 ).ToList();

            return lstGroupMembers;
        }
        public static List<GroupMember> GetAllMembersInGroup(Group group)
        {
            List<GroupMember> lst = new List<GroupMember>();
            
            DataRow groupDataRow = DataSet.Tables["tblGroups"]
                .AsEnumerable()
                .FirstOrDefault(g => g.Field<int>("Id") == group.Id);

            DataRow[] allGroupMembers = groupDataRow?.GetChildRows(RelationGroupMemberToGroup);

            lst = allGroupMembers?.Select(gmRow => new GroupMember(
                    GetPersonById(gmRow.Field<int>("PER_ID")),
                    group 
                )).ToList();

            return lst;
        }
        public static List<GroupMember> GetAllGroupsOfPerson(Person person)
        {
            List<GroupMember> lst = new List<GroupMember>();

            DataRow personDataRow = DataSet.Tables["tblPersons"]
                .AsEnumerable()
                .FirstOrDefault(g => g.Field<int>("Id") == person.Id);

            DataRow[] allGroups = personDataRow?.GetChildRows(RelationGroupMemberToPerson);

            lst = allGroups?.Select(gmRow => new GroupMember(
                    person,
                    GetGroupById(gmRow.Field<int>("GRP_ID"))
                )).ToList();

            return lst;
        }
        private static void RemoveGroupMember(GroupMember groupMember)
        {
            DataRow row;
            try
            {
                row = DataSet.Tables["tblGroupMembers"]
                    .AsEnumerable()
                    .FirstOrDefault(dataRow => dataRow.Field<int>("PER_ID") == groupMember.Person.Id
                                               && dataRow.Field<int>("GRP_ID") == groupMember.Group.Id
                     );

                row?.Delete();
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR RemoveGroupMember" + ex.Message);
            }
            finally
            {
                SaveChangesToDb();
            }
        }
        private static void RemoveGroupMembers(List<GroupMember> list)
        {
            list.ForEach(RemoveGroupMember);
        }

    }
}
